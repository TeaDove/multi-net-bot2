﻿#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdio.h>
#include <string>
#include <cstring>
#include <chrono>
#include <ctime>
#include <thread>
#include <vector>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/encodedstream.h"
#include "rapidjson/memorystream.h"

#include "cwrap.h" //Моя либа-обёртка для libcurl, а также другие полезные методы.
#include "vkwrap.h" //Поисковик для ВК
#include "youtubewrap.h" //Поисковик для Ютуба

using namespace std;
using namespace rapidjson;

struct userData { //Структура отдельного телеграмм юзера, нужна для сохранения информации о выборе юзера.
    int amount_of_posts = 1;
    map<string, bool> services{ {"youtube",false},{"vk",true} };//Карта, в котором хранится выбор юзера о сервисах
};

int main() {
    bool flagInitSuccess = true;
    cout << "Hello World" << endl;
    ifstream cFile("../config.json");
    if (cFile.is_open()) {
        string tmpConfig((std::istreambuf_iterator<char>(cFile)),
            std::istreambuf_iterator<char>());
        cFile.close();
        Document myConfig;
        myConfig.Parse(&tmpConfig[0]); // Файл конфигурации

        string vkToken = myConfig["vk-token"].GetString();
        string youtubeToken = myConfig["youtube-token"].GetString();
        string telegramToken = myConfig["telegram-token"].GetString();
        string botOwner = myConfig["owner"].GetString();

        cout << "Config parse OK" << endl;
        Document botJson;
        string readBuffer = cwrap::req("https://api.telegram.org/bot" + telegramToken + "/getMe");
        if (readBuffer == "") {
            cout << "Bad internet connection, cannot connect to telegram, probably you are in Russia and dont have VPN!!!"<<endl;
            cout << readBuffer;
            flagInitSuccess = false;
        }
        else {
            botJson.Parse(&readBuffer[0]);
            if (botJson["ok"].GetBool())
                cout << "Telegram api OK" << endl;
            else {
                cout << "Telegram api unseccess!!!" << endl;
                cout << readBuffer;
                flagInitSuccess = false;
            }
        }
        readBuffer = cwrap::req(
            "https://api.vk.com/method/users.get?user_ids=0&fields=bdate&access_token=" + vkToken + "&v=5.103");
        botJson.Parse(&readBuffer[0]); 
        if (readBuffer == "") {
            cout << "Bad internet connection, cannot connect to vk!!!"<<endl;
            cout << readBuffer;
            flagInitSuccess = false;
        }
        else {
            if (!botJson.HasMember("error"))
                cout << "VK api OK" << endl;
            else {
                cout << "VK api unseccess!!!" << endl;
                cout << readBuffer;
                flagInitSuccess = false;
            }
        }
        readBuffer = cwrap::req(
            "https://www.googleapis.com/youtube/v3/search?part=snippet&q=kill%20la%20kill&key=" + youtubeToken);
        if (readBuffer == "") {
            cout << "Bad internet connection, cannot connect to youtube!!!"<<endl;
            cout << readBuffer;
            flagInitSuccess = false;
        }
        else {
            botJson.Parse(&readBuffer[0]);
            if (!botJson.HasMember("error"))
                cout << "Youtube api OK" << endl;
            else {
                cout << "Youtube api unseccess!!!" << endl;
                cout << readBuffer;
                		flagInitSuccess = false;
            }
        }
        readBuffer = cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
            "chat_id=" + botOwner + "&text=<b>Hello World</b>&parse_mode=html");
        if (readBuffer == "") {
            cout << "Bad internet connection, cannot connect to telegram, probably you are in Russia and dont have VPN!!!"<<endl;
            cout << readBuffer;
            flagInitSuccess = false;
        }
        else {
            botJson.Parse(&readBuffer[0]);
            if (botJson["ok"].GetBool())
                cout << "Telegram owner OK" << endl;
            else {
                cout << "Telegram owner unseccess!!!" << endl;
                cout << readBuffer;
                flagInitSuccess = false;
            }
        }
        if (!flagInitSuccess)
            cout << "Initialization unseccess!!!" << endl;
        else {
            cout << "Initialization OK" << endl;
            int curUpdateId = 0;
            bool flagWhile = true;
            while (flagWhile) {//Ждём первый апдейт
                //this_thread::sleep_for(chrono::milliseconds(1000));//Дилей на 1 секунду
                readBuffer = cwrap::req("https://api.telegram.org/bot" + telegramToken + "/getUpdates",
                    "allowed_updates=[\"message\"]&limit=1");//Получить первый апдейт, проигорить его и запомнить номер его, дабы работать с ним далее.
                botJson.Parse(&readBuffer[0]);
                cout << "Awaiting first update" << endl;
                //cout << readBuffer << endl;
                        //cout<<(botJson["result"].Size()!=0)<<endl;
                if (botJson["result"].Size() != 0) { //Проверяем на пустоту result
                    const Value& a = botJson["result"];
                    curUpdateId = a[0]["update_id"].GetInt();
                    cout << "First update OK" << endl;
                    flagWhile = false;
                }
            }
            map<int, userData> userDict;//Словарь всех юзеров бота по их айдишникам
            while (true) {    //Диспатчер апдейтов с бота телеграмма
                //time_t my_time = time(NULL);
                //cout << ctime(&my_time);
                //this_thread::sleep_for(chrono::milliseconds(1000));//Дилей на 1 секунду
                readBuffer = cwrap::req("https://api.telegram.org/bot" + telegramToken + "/getUpdates",
                    "allowed_updates=[\"message\",\"callback_query\"]&offset=" +
                    to_string(curUpdateId) + "&limit=1");
                //cout<<"-----"<<endl;
        //cout<<"https://api.telegram.org/bot" << telegramToken << "/getUpdates" << "?allowed_updates=[\"message\",\"callback_query\"]&offset=" <<curUpdateId << "&limit=1"<<endl;
        //cout<<readBuffer<<endl;
        //cout<<"-----"<<endl;
                botJson.Parse(&readBuffer[0]);
                cout << "Checking update #" << curUpdateId << " ";
                if (botJson["result"].Size() == 0)
                    cout << "No new updates" << endl;
                else {
                    cout << "Working with new update ";
                    curUpdateId++;
                    const Value& updates = botJson["result"];
                    if (updates[0].HasMember("message")) {
                        const Value& message = updates[0]["message"];
                        if (message.HasMember("text")) {
                            const Value& user = message["from"];
                            string messageText = message["text"].GetString();
                            int userId = user["id"].GetInt();
                            cout << "from " << userId << " ";
                            cout << "text \"" << messageText << "\" ";
                            if (userDict.find(userId) == userDict.end()) {
                                userDict[userId] = {};
                            }
                            switch (cwrap::str2int(
                                &messageText[0])) { //cwrap::str2int() позволяет использовать стринг в свиче
                            case cwrap::str2int("/get_keyboard"):
                            case cwrap::str2int("/start"): {
                                cout << "proccesing /get_keyboard" << endl;
                                string strToSend = "Hello, i\'m Multi Net Bot, i will help you with surfing through social networks, for more information, send command <b>/help</b>";
                                cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                    "chat_id=" + to_string(userId) + "&text=" + strToSend +
                                    "&parse_mode=html&reply_markup={\"keyboard\":[[\"/Set services\"],[\"/Posts 1\",\"/Posts 3\",\"/Posts 5\"],[\"/Help\",\"/About\"]],\"resize_keyboard\":true}");
                                //userDict[userId].flagSet = false;
                                break;
                            }
                            case cwrap::str2int("/Help"):
                            case cwrap::str2int("/help"): {
                                cout << "proccesing /help" << endl;
                                string strToSend = "How to use: send any text, and you will get posts from social networks!%0A<b>/help</b> - show this text%0A<b>/set_services</b> - set which social networks you want to use%0A/<b>Posts N</b> - set Number of posts you want to see from one request%0A";
                                cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                    "chat_id=" + to_string(userId) + "&text=" + strToSend +
                                    "&parse_mode=html");
                                //userDict[userId].flagSet = false;
                                break;
                            }
                            case cwrap::str2int("/About"):
                            case cwrap::str2int("/about"): {
                                cout << "proccesing /about" << endl;
                                string strToSend = "Little project by Peter Ibragimov @teadove%0ASourse code: https://gitlab.com/teadove/multi-net-bot2";
                                cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                    "chat_id=" + to_string(userId) + "&text=" + strToSend +
                                    "&parse_mode=html");
                                //userDict[userId].flagSet = false;
                                break;
                            }
                            case cwrap::str2int("/Set services"):
                            case cwrap::str2int("/set services"): {
                                cout << "proccesing /set_services" << endl;
                                string strToSend = "Choose social networks you want to search in!";
                                string bodyToSend = "chat_id=" + to_string(userId) + "&text=" + strToSend +
                                    "&parse_mode=html&reply_markup={\"inline_keyboard\":[[{\"text\":\"Youtube " +
                                    (userDict[userId].services["youtube"] ? "selected"
                                        : "not selected") +
                                    "\",\"callback_data\":\"youtube\"}],[{\"text\":\"VK " +
                                    (userDict[userId].services["vk"] ? "selected"
                                        : "not selected") +
                                    "\",\"callback_data\":\"vk\"}]]}";
                                cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                    bodyToSend);
                                //cout<<endl<<bodyToSend;
                                //userDict[userId].flagSet = false;
                                break;
                            }
                            default:
                                cout << "proccesing default ";
                                if ((messageText.substr(0, 7) == "/Posts ") && (messageText.size() == 8)) {
                                    int newValue = atoi(&messageText[7]);
                                    if (newValue > 0) {
                                        cout << "chaging amount of posts" << endl;
                                        string strToSend = "Your new setting is " + to_string(newValue);
                                        cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                            "chat_id=" + to_string(userId) + "&text=" + strToSend +
                                            "&parse_mode=html");
                                        userDict[userId].amount_of_posts = newValue;
                                    }
                                    else
                                        goto goto_searching;//Да, я это использовал, Не бейте, оно обоснованно!!!
                                }
                                else {//Блок поиска постов, самое важное!!!
                                goto_searching:
                                    cout << "searching for posts" << endl;
                                    string strToSend =
                                        "Searching for posts with <b>\"" + messageText + "\"</b> in:%0A";
                                    for (map<string, bool>::iterator it = userDict[userId].services.begin();
                                        it != userDict[userId].services.end(); it++)
                                        if (it->second)
                                            strToSend += "<b>" + it->first + "</b>%0A";
                                    strToSend += "%0Afor " + to_string(userDict[userId].amount_of_posts) + " posts";
                                    cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                        "chat_id=" + to_string(userId) + "&text=" + strToSend +
                                        "&parse_mode=html");
                                    vector <string> urls;
                                    vector <string> tempUrls;
                                    //                                        if (userDict[userId].services["twitter"]) {}
                                    if (userDict[userId].services["youtube"]) {
                                        tempUrls = youtubewrap::search(messageText, userDict[userId].amount_of_posts,
                                            youtubeToken);
                                        urls.insert(urls.end(), tempUrls.begin(), tempUrls.end());
                                    }
                                    if (userDict[userId].services["vk"]) {
                                        tempUrls = vkwrap::search(messageText, userDict[userId].amount_of_posts,
                                            vkToken);
                                        urls.insert(urls.end(), tempUrls.begin(), tempUrls.end());
                                    }
                                    bool flagEmpty = true;
                                    for (int i = 0; i < urls.size(); i++) {
                                        if (urls[i] != "0") {
                                            flagEmpty = false;
                                            //cout << urls[i] << endl;
                                            cwrap::req(
                                                "https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                                "chat_id=" + to_string(userId) + "&text=" + urls[i]);
                                        }
                                    }
                                    if (flagEmpty)
                                        cwrap::req("https://api.telegram.org/bot" + telegramToken + "/sendMessage",
                                            "chat_id=" + to_string(userId) +
                                            "&text=sorry, no posts were found");

                                    //TODO Блок поиска постов

                                }
                                //userDict[userId].flagSet = false;
                                break;
                            }
                        }
                        else
                            cout << "not text update" << endl;//Игнорировать в случае не текстовых сообщений
                    }
                    else if (updates[0].HasMember("callback_query")) {
                        const Value& callback_query = updates[0]["callback_query"];
                        if (callback_query.HasMember("message")) {
                            const Value& message = callback_query["message"];
                            int message_id = message["message_id"].GetInt();
                            string callback_data = callback_query["data"].GetString();
                            const Value& user = callback_query["from"];
                            int userId = user["id"].GetInt();
                            cout << "from " << userId << " ";
                            cout << "callback_data \"" << callback_data << "\" ";
                            if (userDict.find(userId) == userDict.end()) {
                                userDict[userId] = {};
                            }
                            bool flagData = false;
                            for (map<string, bool>::iterator it = userDict[userId].services.begin();
                                it != userDict[userId].services.end(); it++)
                                if (it->first == callback_data) {
                                    flagData = true;
                                    it->second = !(it->second);
                                }
                            if (flagData) {
                                string bodyToSend =
                                    "chat_id=" + to_string(userId) + "&message_id=" + to_string(message_id) +
                                    "&reply_markup={\"inline_keyboard\":[[{\"text\":\"Youtube " +
                                    (userDict[userId].services["youtube"] ? "selected"
                                        : "not selected") +
                                    "\",\"callback_data\":\"youtube\"}],[{\"text\":\"VK " +
                                    (userDict[userId].services["vk"] ? "selected"
                                        : "not selected") +
                                    "\",\"callback_data\":\"vk\"}]]}";
                                cwrap::req("https://api.telegram.org/bot" + telegramToken + "/editMessageReplyMarkup",
                                    bodyToSend);
                            }
                            else {
                                cout << "wrong callback_data" << endl;
                            }
                            string callback_id = callback_query["id"].GetString();
                            cwrap::req("https://api.telegram.org/bot" + telegramToken + "/answerCallbackQuery",
                                "callback_query_id=" + callback_id);
                        }
                        else
                            cout << "no message callback_query" << endl;//Игнорировать если нет сообщения
                    }
                }
            }

        }
    }
    else {
        std::cerr << "Couldn't open config file for reading.\n";
        cout << "Initialization unseccess!!!" << endl;
    }
    return 0;
}



//TODO понять что это блин
