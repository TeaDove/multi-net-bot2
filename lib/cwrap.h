#include <stdio.h>
#include <string>
#include <cstring>
#include "curl/curl.h"

class cwrap{
    private:
	static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) //Понятия не имею что это делает, но без это функции программа не работает
	{
		((std::string*)userp)->append((char*)contents, size * nmemb);
		return size * nmemb;
	}
    public:
	static std::string req(std::string URL){ //Метод GET запроса, даёте URL, выдаёт ответ сервера в string
	    if (URL.find(' ')!=std::string::npos){ //Так как запрос GET, а не POST, пробелы использовать нельзя и их надо заменить на %20
            	size_t index = 0;
            	while (true) {
                	/* Locate the substring to replace. */
                	index = URL.find(" ", index);
                	if (index == std::string::npos) break;

                	/* Make the replacement. */
                	URL.replace(index,1,"%20");

                	/* Advance index forward so the next iteration doesn't pick it up as well. */
                	index+=3;
            	}
	    }

	    CURL *curl;
	    CURLcode res;
	    curl = curl_easy_init();
	    std::string readBuffer;	
	    curl_easy_setopt(curl, CURLOPT_URL,&URL[0]);
	    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
	    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
	    curl_easy_perform(curl);
	    curl_easy_cleanup(curl);
	    curl_global_cleanup();
	    return readBuffer;	
	}

	static std::string req(const std::string& URL,const std::string& BODY){ //Метод POST запроса, даёте URL и BODY, выдаёт ответ сервера в string
		CURL *curl;
		CURLcode res;
		curl = curl_easy_init();
		std::string readBuffer;	
		curl_easy_setopt(curl, CURLOPT_URL,&URL[0]);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, &BODY[0]);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		curl_global_cleanup();
		return readBuffer;	
	}

	static constexpr unsigned int str2int(const char* str, int h = 0){ //Недо хэширование, нужно для работы свича со строками
		    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
	}
};
