#include <vector>
#include <string>

//#include "cwrap.h"

using namespace rapidjson;
using namespace std;

class vkwrap {
public:
    static vector<string> search(string text_for_req, int amount, string vkToken) {
        vector<string> toReturn{"0"};
        string next_from = "0";
        while (amount != 0) {
            string readRes = cwrap::req("https://api.vk.com/method/newsfeed.search",
                                        "q=" + text_for_req + "&count=1&access_token=" + vkToken +
                                        "&v=5.103&start_from="+next_from);
            Document resJson;
            resJson.Parse(&readRes[0]);
//            cout <<"-----"<<endl<<readRes<<endl;
            if (resJson.HasMember("response")) {
                const Value &response = resJson["response"];
                const Value &items = response["items"];
                if (items.Size() == 0) {
                    amount = 0;
                } //Не было найденно результатов более
                else {
                    const Value &item = items[0];
                    if (item["post_type"] == "post") {
                        amount--;
//                        cout<<"vk.com/wall"<<item["owner_id"].GetInt64()<<"_"<<item["id"].GetInt64()<<endl;
                        toReturn.push_back("vk.com/wall"+to_string(item["owner_id"].GetInt64()) + "_" + to_string(item["id"].GetInt64()));
                    }
                    if (response.HasMember("next_from"))
                        next_from = response["next_from"].GetString();
                    else
                        amount = 0; //Этот результат последний
                }
            }
        }
        return toReturn;
    }
};
