#include <vector>
#include <string>

//#include "cwrap.h"

using namespace rapidjson;
using namespace std;

class youtubewrap {
public:
    static vector<string> search(string text_for_req, int amount, string youtubeToken) {
        vector<string> toReturn{"0"};
        string pageToken = "";
        while (amount != 0) {
            string readRes = "";
            if (pageToken=="")
                readRes = cwrap::req("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&order=viewCount&type=video&q=" + text_for_req + "&key=" + youtubeToken);
            else
                readRes = cwrap::req("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&order=viewCount&type=video&q=" + text_for_req + "&key=" + youtubeToken + "&pageToken="+pageToken);
            Document resJson;
            resJson.Parse(&readRes[0]);
            //cout <<"-----"<<endl<<readRes<<endl;
            if (resJson.HasMember("items")) {
                const Value &items = resJson["items"];
                if (items.Size() == 0) {
                    amount = 0;
                } //Не было найденно результатов более
                else {
                    const Value &item = items[0];
                    const Value &id = item["id"];
                    amount--;
                    string strToSend = "https://www.youtube.com/watch?v=";
                    strToSend+=id["videoId"].GetString();
//                    cout<<strToSend;
                    toReturn.push_back(strToSend);
                }
                if (resJson.HasMember("nextPageToken"))
                    pageToken = resJson["nextPageToken"].GetString();
                else
                    amount = 0; //Этот результат последний
            }
        }
        return toReturn;
    }
};
